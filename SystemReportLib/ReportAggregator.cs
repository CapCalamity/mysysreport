﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace SystemReportLib
{
    public class ReportAggregator : IDisposable
    {
        private readonly List<IStatusReporter> _reporters;
        private readonly List<Report> _savedReports;
        private readonly Timer _timer;

        public event EventHandler<List<Report>> OnAfterReportsGenerated;

        public ReportAggregator(double interval)
        {
            _reporters = new List<IStatusReporter>();
            _savedReports = new List<Report>();
            _timer = new Timer(interval) { AutoReset = false };
            _timer.Elapsed += TimerOnElapsed;
        }

        public void Start() => _timer.Start();

        public IEnumerable<Report> GetMostCurrentReports() => _savedReports.ToArray();

        private void TimerOnElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            var now = DateTime.Now;
            var reports = _reporters.Select(reporter => new Report(reporter.Name,
                                                                   reporter.NextValue ?? new Dictionary<string, object>(),
                                                            now))
                                    .ToList();

            _savedReports.Clear();
            _savedReports.AddRange(reports);
            OnAfterReportsGenerated?.Invoke(this, reports);

            Start();
        }

        public void AddReporter(IStatusReporter reporter) => _reporters.Add(reporter);

        public void Dispose()
        {
            _reporters.Clear();
            _savedReports.Clear();
            _timer.Dispose();
        }
    }
}