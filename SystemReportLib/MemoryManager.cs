﻿using System;
using SystemReportLib.Extensions;

namespace SystemReportLib
{
    public static class MemoryManager
    {
        public static Int64 TotalMemory
        {
            get
            {
                Refresh();
                return _totalMemory;
            }
        }

        public static Int64 AvailableMemory
        {
            get
            {
                Refresh();
                return _availableMemory;
            }
        }

        public static decimal FreeMemoryPercentage
        {
            get
            {
                Refresh();
                return _freeMemoryPercentage.Round();
            }
        }

        public static decimal OccupiedMemoryPercentage
        {
            get
            {
                Refresh();
                return _occupiedMemoryPercentage.Round();
            }
        }

        private static Int64 _totalMemory;
        private static Int64 _availableMemory;
        private static decimal _freeMemoryPercentage;
        private static decimal _occupiedMemoryPercentage;

        private static DateTime _lastRefresh = new DateTime(0);

        private static void Refresh()
        {
            if ((DateTime.Now - _lastRefresh) <= TimeSpan.FromSeconds(1.0))
                return;

            _availableMemory = NativeMethods.GetPhysicalAvailableMemoryInMiB();
            _totalMemory = NativeMethods.GetTotalMemoryInMiB();
            _freeMemoryPercentage = ((decimal) _availableMemory / (decimal) _totalMemory) * 100;
            _occupiedMemoryPercentage = 100 - _freeMemoryPercentage;

            _lastRefresh = DateTime.Now;
        }
    }
}
