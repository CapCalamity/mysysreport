﻿using System;

namespace SystemReportLib.Extensions
{
    public static class Extensions
    {
        public static float Round(this float value, int digits = 2, MidpointRounding mode = MidpointRounding.AwayFromZero)
            => (float) Math.Round(value, digits, mode);

        public static double Round(this double value, int digits = 2, MidpointRounding mode = MidpointRounding.AwayFromZero)
            => Math.Round(value, digits, mode);

        public static decimal Round(this decimal value, int digits = 2, MidpointRounding mode = MidpointRounding.AwayFromZero)
            => Math.Round(value, digits, mode);
    }
}