﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using Timer = System.Timers.Timer;

namespace SystemReportLib
{
    public class ReportDistributor
    {
        private int _pipeInterval;
        public int PipeInterval
        {
            get { return (_pipeInterval < PipeIntervalMinValue ? PipeIntervalMinValue : _pipeInterval); }
            set { _pipeInterval = (value > PipeIntervalMinValue ? value : PipeIntervalMinValue); }
        }

        private int _serverInterval;
        public int ServerInterval
        {
            get { return (_serverInterval < ServerIntervalMinValue ? ServerIntervalMinValue : _serverInterval); }
            set { _serverInterval = (value > 1000 ? value : 1000); }
        }

        private Uri _serverUri;
        public Uri ServerUri
        {
            get { return _serverUri; }
            set { _serverUri = value; }
        }

        private readonly Func<IEnumerable<Report>> _getMostCurrentReports;
        private readonly Timer _serverTimer;

        private const string PipeNameCurrent = "MSysRepCurrent";
        private const int MaxPipeConnections = 254;
        private const int ServerIntervalMinValue = 1000;
        private const int PipeIntervalMinValue = 100;

        private static readonly byte[] EmptyLineBytes = Encoding.UTF8.GetBytes("\r\n");

        public ReportDistributor(Func<IEnumerable<Report>> getMostCurrentReports)
        {
            _pipeInterval = 1000;
            _serverInterval = 1000;
            _getMostCurrentReports = getMostCurrentReports;
            _serverTimer = new Timer
            {
                AutoReset = true,
                Interval = _serverInterval
            };
            _serverTimer.Elapsed += (sender, eArgs) => UploadStats();
        }

        /// <summary>
        ///     http://stackoverflow.com/questions/11450522/c-asynchronous-namedpipeserverstream-the-pipe-is-being-closed-exception
        /// </summary>
        public void ListenForClients()
        {
            AcceptClientForCurrentReports();
        }

        public void StartServerUpload() => _serverTimer?.Start();

        public void StopServerUpload() => _serverTimer?.Stop();

        private void UploadStats()
        {
        }

        private void AcceptClientForCurrentReports()
        {
            try
            {
                var pipeServer = new NamedPipeServerStream(PipeNameCurrent, PipeDirection.InOut, MaxPipeConnections, PipeTransmissionMode.Byte, PipeOptions.Asynchronous);
                pipeServer.BeginWaitForConnection(asyncResult =>
                {
                    using (var connection = (NamedPipeServerStream) asyncResult.AsyncState)
                    {
                        try
                        {
                            connection.EndWaitForConnection(asyncResult);
                        }
                        catch (Exception)
                        {
                            return;
                        }
                        finally
                        {
                            AcceptClientForCurrentReports();
                        }

                        while (connection.IsConnected && connection.CanWrite)
                        {
                            foreach (var report in _getMostCurrentReports())
                            {
                                var bytes = Encoding.UTF8.GetBytes(report.ToString());
                                connection.WriteAsync(bytes, 0, bytes.Length);
                                connection.WriteAsync(EmptyLineBytes, 0, EmptyLineBytes.Length);
                            }

                            connection.WriteAsync(EmptyLineBytes, 0, EmptyLineBytes.Length);

                            Thread.Sleep(_pipeInterval);
                        }
                    }
                }, pipeServer);
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("{0}:{1}\r\n{2}", e.GetType(), e.Message, e.StackTrace);
#endif
            }
        }
    }
}
