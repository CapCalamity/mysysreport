﻿using System.Collections.Generic;

namespace SystemReportLib
{
    public interface IStatusReporter
    {
        /// <summary>
        /// The Unique, Identifying name of this Report
        /// </summary>
        string Name { get; }

        /// <summary>
        /// A shorter representation of this Report
        /// </summary>
        string ShortName { get; }
        
        /// <summary>
        /// The next value this Report has to offer.
        /// </summary>
        IDictionary<string, object> NextValue { get; }
    }
}