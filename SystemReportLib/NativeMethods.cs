﻿using System;
using System.Runtime.InteropServices;

namespace SystemReportLib
{
    internal class NativeMethods
    {
        private const int MByte = 1024 * 1024;

        public static Int64 GetPhysicalAvailableMemoryInMiB()
        {
            var pi = new PerformanceInfo.PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
                return Convert.ToInt64((pi.PhysicalAvailable.ToInt64() * pi.PageSize.ToInt64() / MByte));
            return -1;
        }

        public static Int64 GetTotalMemoryInMiB()
        {
            var pi = new PerformanceInfo.PerformanceInformation();
            if (GetPerformanceInfo(out pi, Marshal.SizeOf(pi)))
                return Convert.ToInt64((pi.PhysicalTotal.ToInt64() * pi.PageSize.ToInt64() / MByte));
            return -1;
        }

        [DllImport("psapi.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetPerformanceInfo([Out] out PerformanceInfo.PerformanceInformation performanceInformation, [In] int size);
    }
}
