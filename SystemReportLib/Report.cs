﻿using System;
using System.Collections.Generic;

namespace SystemReportLib
{
    public class Report
    {
        public string CreatedBy { get; }
        public IDictionary<string, object> Value { get; }
        public DateTime CreationTime { get; }

        public Report(string createdBy, IDictionary<string, object> value, DateTime creationTime)
        {
            CreationTime = creationTime;
            Value = value;
            CreatedBy = createdBy;
        }

        public override string ToString() => $"{CreationTime} : {CreatedBy} : {Value.Count}";

        public override int GetHashCode() => CreatedBy.GetHashCode() + Value.GetHashCode() + CreationTime.GetHashCode();

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (!(obj is Report other))
                return false;

            return CreatedBy.Equals(other.CreatedBy)
                   && Value.Equals(other.Value)
                   && CreationTime.Equals(other.CreationTime);
        }
    }
}