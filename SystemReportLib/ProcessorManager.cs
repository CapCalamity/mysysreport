﻿using System.Linq;
using System.Management;

namespace SystemReportLib
{
    public static class ProcessorManager
    {
        //http://msdn.microsoft.com/en-us/library/aa394373%28v=vs.85%29.aspx
        public static uint MaxClockSpeed
        {
            get { return (uint) SearchFor("select MaxClockSpeed from Win32_Processor", "MaxClockSpeed"); }
        }

        public static uint CurrentClockSpeed
        {
            get { return (uint) SearchFor("select CurrentClockSpeed from Win32_Processor", "CurrentClockSpeed"); }
        }

        public static uint NumberOfCores
        {
            get { return (uint) SearchFor("select NumberOfCores from Win32_Processor", "NumberOfCores"); }
        }

        public static uint NumberOfLogicalProcessors
        {
            get { return (uint) SearchFor("select NumberOfLogicalProcessors from Win32_Processor", "NumberOfLogicalProcessors"); }
        }

        private static object SearchFor(string query, string field)
        {
            var searcher = new ManagementObjectSearcher(query);
            return (from ManagementBaseObject item in searcher.Get() select (uint) item[field]).FirstOrDefault();
        }
    }
}
