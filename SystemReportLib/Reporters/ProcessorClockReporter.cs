﻿using System.Collections.Generic;

namespace SystemReportLib.Reporters
{
    public class ProcessorClockReporter : IStatusReporter
    {
        public ProcessorClockReporter()
        {
            Name = "Processor Max/Current Clock Speed (MHz)";
            ShortName = "Proc Clock";
        }

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object>
        {
            {"processor_clock_speed_max", ProcessorManager.CurrentClockSpeed},
            {"processor_clock_speed_current", ProcessorManager.MaxClockSpeed}
        };

        public string ShortName { get; }
    }
}