﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SystemReportLib.Reporters
{
    public class ProcessReporter : IStatusReporter
    {
        public string Name { get; }
        public string ShortName { get; }

        public IDictionary<string, object> NextValue => Process.GetProcesses()
                                                               .ToArray()
                                                               .Select(process => new
                                                               {
#pragma warning disable IDE0037 // Use inferred member name
                                                                   Arguments = process.StartInfo.Arguments,
                                                                   Domain = process.StartInfo.Domain,
                                                                   Executable = process.StartInfo.FileName,
                                                                   HandleCount = process.HandleCount,
                                                                   Id = process.Id,
                                                                   MachineName = process.MachineName,
                                                                   MainWindowHandle = process.MainWindowHandle,
                                                                   MainWindowTitle = process.MainWindowTitle,
                                                                   Name = process.ProcessName,
                                                                   PeakWorkingSet = process.PeakWorkingSet64,
                                                                   Priority = process.BasePriority,
                                                                   Threads = process.Threads.Count,
                                                                   User = process.StartInfo.UserName,
                                                                   WorkingDirectory = process.StartInfo.WorkingDirectory,
                                                                   WorkingSet = process.WorkingSet64,
#pragma warning restore IDE0037 // Use inferred member name
                                                               })
                                                               .ToDictionary(e => e.Id.ToString(), e => (object) e);

        public ProcessReporter()
        {
            Name = "Process List";
            ShortName = "Processes";
        }
    }
}