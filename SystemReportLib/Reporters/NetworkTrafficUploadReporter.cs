﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SystemReportLib.Reporters
{
    public class NetworkTrafficUploadReporter : IStatusReporter, IDisposable
    {
        private readonly PerformanceCounter _counter;

        public NetworkTrafficUploadReporter(string networkInterface)
        {
            Name = $"{networkInterface} | Network Traffic Bytes Sent/sec";
            ShortName = $"{networkInterface} Snt/sec";
            _counter = new PerformanceCounter("Network Interface", "Bytes Sent/sec", networkInterface);
        }

        public void Dispose() => _counter?.Dispose();

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object> {{"network_traffic_sent", _counter.NextValue()}};

        public string ShortName { get; }
    }
}