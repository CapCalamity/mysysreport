﻿using System.Collections.Generic;

namespace SystemReportLib.Reporters
{
    public class MemoryTotalReporter : IStatusReporter
    {
        public MemoryTotalReporter()
        {
            Name = "Total Memory (MiB)";
            ShortName = "Mem Tot. (MiB)";
        }

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object> {{"memory_total", MemoryManager.TotalMemory}};

        public string ShortName { get; }
    }
}