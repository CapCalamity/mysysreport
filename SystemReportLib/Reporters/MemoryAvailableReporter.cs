﻿using System.Collections.Generic;

namespace SystemReportLib.Reporters
{
    public class MemoryAvailableReporter : IStatusReporter
    {
        public MemoryAvailableReporter()
        {
            Name = "Memory Available (MiB)";
            ShortName = "Mem Av. (MiB)";
        }

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object> {{"memory_available", MemoryManager.AvailableMemory}};

        public string ShortName { get; }
    }
}