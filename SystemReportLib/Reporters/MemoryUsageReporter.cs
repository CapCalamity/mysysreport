﻿using System.Collections.Generic;

namespace SystemReportLib.Reporters
{
    public class MemoryUsageReporter : IStatusReporter
    {
        public MemoryUsageReporter()
        {
            Name = "Memory Usage %";
            ShortName = "Mem Usage %";
        }

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object> {{"memory_usage", MemoryManager.OccupiedMemoryPercentage}};

        public string ShortName { get; }
    }
}