﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SystemReportLib.Extensions;

namespace SystemReportLib.Reporters
{
    public class ProcessorUsageReporter : IStatusReporter, IDisposable
    {
        private readonly PerformanceCounter _counter;

        public ProcessorUsageReporter()
        {
            Name = "CPU Usage %";
            ShortName = "CPU Usage";
            _counter = new PerformanceCounter("Processor Information", "% Processor Time", "_Total", true);
        }

        public void Dispose() => _counter?.Dispose();

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object> {{"processor_usage", _counter?.NextValue().Round() ?? 0}};

        public string ShortName { get; }
    }
}