﻿using System.Collections.Generic;

namespace SystemReportLib.Reporters
{
    public class ProcessorCoreReporter : IStatusReporter
    {
        public ProcessorCoreReporter()
        {
            Name = "Number of Cores / Logical Processors";
            ShortName = "Phscl/Log Cores";
        }

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object>
        {
            {"processor_cores", ProcessorManager.NumberOfCores},
            {"processor_cores_logical", ProcessorManager.NumberOfLogicalProcessors}
        };

        public string ShortName { get; }
    }
}