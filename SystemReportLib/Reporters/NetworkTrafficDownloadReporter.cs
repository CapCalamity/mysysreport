﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SystemReportLib.Reporters
{
    public class NetworkTrafficDownloadReporter : IStatusReporter, IDisposable
    {
        private readonly PerformanceCounter _counter;

        public NetworkTrafficDownloadReporter(string networkInterface)
        {
            Name = $"{networkInterface} | Network Traffic Bytes Received/sec";
            ShortName = $"{networkInterface} Rcv/sec";
            _counter = new PerformanceCounter("Network Interface", "Bytes Received/sec", networkInterface);
        }

        public void Dispose() => _counter?.Dispose();

        public string Name { get; }

        public IDictionary<string, object> NextValue => new Dictionary<string, object> {{"network_traffic_received", _counter.NextValue()}};

        public string ShortName { get; }
    }
}