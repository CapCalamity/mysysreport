# MSysRep

MSysRep is a project to make reading of performance data easier and more streamlined for developers.

The basic idea is to have a `ReportAggregator` that collects the desired performance metrics and makes them available in set intervals.

**Example**

```
#!csharp
    _distributor = new ReportDistributor(
        () => _aggregator.GetMostCurrentReports(),
        () => _aggregator.GetAllReports().ToDictionary(kvp => kvp.Key, kvp => (IEnumerable<Report>) kvp.Value));

    _aggregator = new ReportAggregator(1000.0, 60);
    _aggregator.OnAfterReportsGenerated += OnAfterReportsGenerated;


    _aggregator.AddReporter(new ProcessorUsageReporter());
    _aggregator.AddReporter(new MemoryUsageReporter());
    _aggregator.AddReporter(new MemoryTotalReporter());
    _aggregator.AddReporter(new MemoryAvailableReporter());
```

## MSysRepLib

This is the library containing the core functionality used in MSysRep.

This dll can be used to provide the same functionality to other programs without using another runing executable.

