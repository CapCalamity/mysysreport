﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using SystemReportLib;
using SystemReportLib.Reporters;

namespace SystemReportCLI
{
    internal class Controller : IDisposable
    {
        private readonly object _runAppLock = new object();
        private bool _runApplication;

        private readonly ReportAggregator _aggregator;
        private readonly ReportDistributor _distributor;

        public Controller()
        {
            _runApplication = true;

            _aggregator = new ReportAggregator(1000.0);
            _aggregator.OnAfterReportsGenerated += OnAfterReportsGenerated;

            _distributor = new ReportDistributor(() => _aggregator.GetMostCurrentReports());
        }

        private void OnAfterReportsGenerated(object sender, List<Report> newReports)
        {
            try
            {
                Console.CursorVisible = false;
                Console.Clear();
                newReports.ForEach(report =>
                {
                    Console.WriteLine(report.CreationTime);
                    Console.WriteLine(report.CreatedBy);
                    foreach (var kvp in report.Value)
                    {
                        Console.WriteLine("\t{0}: {1}", kvp.Key, kvp.Value);
                        Console.WriteLine();
                    }
                });
            }
            catch (Exception)
            {
                // do nothing, this is simply for debugging purposes
#if DEBUG
                throw;
#endif
            }
        }

        private void InitializeReporters()
        {
            _aggregator.AddReporter(new ProcessorUsageReporter());
            _aggregator.AddReporter(new MemoryUsageReporter());
            _aggregator.AddReporter(new MemoryTotalReporter());
            _aggregator.AddReporter(new MemoryAvailableReporter());
            _aggregator.AddReporter(new ProcessorClockReporter());
            _aggregator.AddReporter(new ProcessorCoreReporter());

            foreach (var nic in new PerformanceCounterCategory("Network Interface").GetInstanceNames())
            {
                _aggregator.AddReporter((new NetworkTrafficUploadReporter(nic)));
                _aggregator.AddReporter((new NetworkTrafficDownloadReporter(nic)));
            }

            _aggregator.AddReporter(new ProcessReporter());
        }

        /// <summary>
        /// Start the main-application
        /// <para/>
        /// This will block until the application is either forcibly stopped, the method <seealso cref="Stop"/> is called, or the user presses Ctrl+C
        /// </summary>
        public void Start()
        {
            Console.CancelKeyPress += (sender, args) => Stop();

            InitializeReporters();

            _aggregator.Start();
            _distributor.ListenForClients();

            var waithandle = new EventWaitHandle(false, EventResetMode.ManualReset, "MySysRep");

            while (_runApplication)
            {
                waithandle.WaitOne(TimeSpan.FromMilliseconds(50));
                waithandle.Reset();
            }
        }

        public void Stop()
        {
            lock (_runAppLock)
            {
                _runApplication = false;
            }
        }

        public void Dispose()
        {
            _aggregator.Dispose();
        }
    }
}