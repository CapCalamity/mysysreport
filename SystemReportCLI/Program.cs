﻿namespace SystemReportCLI
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            using (var controller = new Controller())
            {
                controller.Start();
            }
        }
    }
}